package ru.innopolis.store.services;

import ru.innopolis.store.model.Order;

import java.util.List;

public interface OrderService {
    /**
     * Найти заказы по идентификатору пользователя
     * @param userId
     * @return
     */

    List<Order> findOrdersByUserId(Integer userId);

    /**
     * Добавить товар
     * @param userId
     * @param productId
     */
    void addOrder(Integer userId, Integer productId);
}
