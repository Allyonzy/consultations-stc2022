package ru.innopolis.store.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.innopolis.store.dto.UserDto;
import ru.innopolis.store.services.SignUpService;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
@RequestMapping("/sign-up")
public class SignUpController {

    private final SignUpService signUpService;

    @GetMapping
    public String getSignUpPage() {
        return "sign-up";
    }

    @PostMapping
    public String signUpCustomer(
            @Valid UserDto userDto,
            BindingResult result,
            RedirectAttributes forRedirectModel
    ) {
        if (result.hasErrors()) {
            forRedirectModel.addFlashAttribute("errors", "Телефон вводится в формате +700000000000");
            return "redirect:/sign-up";
        }
        signUpService.signUpUser(userDto);
        return "redirect:/sign-Un";
    }
}
