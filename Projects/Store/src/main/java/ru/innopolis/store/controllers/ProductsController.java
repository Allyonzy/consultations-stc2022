package ru.innopolis.store.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.innopolis.store.model.Product;
import ru.innopolis.store.services.ProductService;

import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/products")
public class ProductsController {

    @Autowired
    private final ProductService productService;

    @GetMapping
    public String getProductsPage(Model model) {
        List<Product> productList = productService.findAll();
        model.addAttribute("products", productList);
        return "products";
    }

    @PostMapping("/{product-id}/delete")
    public String deleteProduct(@PathVariable("product-id") Integer productId) {
        productService.delete(productId);
        return "redirect:/products";
    }

}
