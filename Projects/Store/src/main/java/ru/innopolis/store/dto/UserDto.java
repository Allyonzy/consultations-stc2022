package ru.innopolis.store.dto;
import org.hibernate.validator.constraints.Length; //дополнение

import lombok.Data;

@Data
public class UserDto {
    private String firstName;
    private String lastName;

    @Length(min = 12, max = 12)
    private String telephone;
    private String email;
    private String address;
    private String password;
}
