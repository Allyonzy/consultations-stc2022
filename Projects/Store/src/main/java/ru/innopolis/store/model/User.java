package ru.innopolis.store.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name="store_user")
public class User {

    public enum Role {
        ADMIN, USER
    }

    @Enumerated(value = EnumType.STRING)
    private Role role;

    /**
     * Идентификатор пользователя
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * Имя пользователя
     */
    @Column(name="first_name")
    private String firstName;

    /**
     * Фамилия пользователя
     */
    @Column(name="last_name")
    private String lastName;

    private String telephone;
    private String email;
    private String address;
    private String hashPassword;

    /**
     * Список заказов пользователя
     */
    @OneToMany(mappedBy = "customer")
    private List<Order> orders;

}
