package ru.innopolis.store.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.innopolis.store.model.Order;
import ru.innopolis.store.model.Product;
import ru.innopolis.store.model.User;
import ru.innopolis.store.repositories.OrderRepository;
import ru.innopolis.store.repositories.ProductRepository;
import ru.innopolis.store.repositories.UserRepository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    @Autowired
    private final OrderRepository orderRepository;

    @Autowired
    private final UserRepository userRepository;

    @Autowired
    private final ProductRepository productRepository;

    @Override
    public List<Order> findOrdersByUserId(Integer userId) {
        return orderRepository.findAllByCustomer_Id(userId);
    }

    @Transactional
    @Override
    public void addOrder(Integer userId, Integer productId) {
        User user = userRepository.findById(userId).orElseThrow(RuntimeException::new);
        Product product = productRepository.findById(productId).orElseThrow(RuntimeException::new);
        Integer count = 1;

        Order newOrder = Order.builder()
                .orderDate(LocalDate.now())
                .count(count)
                .customer(user)
                .product(product)
                .build();

        orderRepository.save(newOrder);

    }
}
