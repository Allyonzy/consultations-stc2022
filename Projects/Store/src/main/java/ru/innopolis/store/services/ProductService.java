package ru.innopolis.store.services;

import ru.innopolis.store.model.Product;

import java.util.List;

public interface ProductService {

    /**
     * Получить все товары
     * @return products список товаров
     */
    List<Product> findAll();

    /**
     * Найти продукт по идентификатору
     * @param id
     * @return
     */
    Product findProductById(Integer id);

    /**
     * Удалить продукт по идентификатору
     * @param id
     */
    void delete(Integer id);

    /**
     * ОБновить продукт
     * @param id
     * @param productForm
     */
    void update(Integer id, Product productForm);
}
