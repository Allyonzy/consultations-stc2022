package ru.innopolis.store.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
@RequestMapping("/sign-in")
public class SignInController {

    @GetMapping
    public String getSignInPage() {
        return "sign-in";
    }
}
