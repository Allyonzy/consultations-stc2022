package ru.innopolis.store.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name="store_product")
public class Product {

    /**
     * Идентификатор продукта
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * Наименование продукта
     */
    private String name;

    /**
     * Цена продукта
     */
    private Double price;

    /**
     * Количество продукта
     */
    private Integer amount;

    /**
     * Список заказов в которых участвует продукт
     * (при условии, что продукт не дублируется и
     * мы увеличиваем счетчик продуктов при добавлении такого же)
     */
    @OneToMany(mappedBy = "product")
    private List<Order> orders;
}
