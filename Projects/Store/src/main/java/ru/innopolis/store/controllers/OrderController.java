package ru.innopolis.store.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.innopolis.store.model.Order;
import ru.innopolis.store.services.OrderService;

import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/orders")
public class OrderController {

    @Autowired
    private final OrderService orderService;

    @GetMapping
    public String getOrderPage() {
        return "orders";
    }

    @GetMapping("/{user-id}")
    public String getOrderByUserId(Model model, @PathVariable("user-id") Integer userId) {
        List<Order> orders = orderService.findOrdersByUserId(userId);
        model.addAttribute("user-id", userId);
        model.addAttribute("orders", orders);
        return "redirect:/orders/" + userId;
    }

    @PostMapping("/{user-id}/{product-id}")
    public String addOrder(
            @PathVariable("user-id") Integer userId,
            @PathVariable("product-id") Integer productId
    ) {
        orderService.addOrder(userId, productId);
        return "redirect:/order/" + userId + "/" + productId;
    }


}
