package ru.innopolis.store.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.innopolis.store.model.Order;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Integer> {
    List<Order> findAllByCustomer_Id(Integer userId);
}
