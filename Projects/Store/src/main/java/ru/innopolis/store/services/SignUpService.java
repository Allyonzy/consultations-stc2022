package ru.innopolis.store.services;

import ru.innopolis.store.dto.UserDto;

public interface SignUpService {
    /**
     * Добавление нового пользователя
     * @param userDto - форма с данными
     */
    void signUpUser(UserDto userDto);
}
