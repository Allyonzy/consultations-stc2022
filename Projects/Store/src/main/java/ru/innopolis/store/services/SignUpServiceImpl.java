package ru.innopolis.store.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.innopolis.store.dto.UserDto;
import ru.innopolis.store.model.User;
import ru.innopolis.store.repositories.UserRepository;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService{
    private final PasswordEncoder passwordEncoder;

    @Autowired
    private final UserRepository userRepository;

    @Transactional
    @Override
    public void signUpUser(UserDto userDto) {
        User newUser = User.builder()
                .firstName(userDto.getFirstName())
                .lastName(userDto.getLastName())
                .telephone(userDto.getTelephone())
                .email(userDto.getEmail())
                .address(userDto.getAddress())
                .role(User.Role.USER)
                .hashPassword(passwordEncoder.encode(userDto.getPassword()))
                .build();

        userRepository.save(newUser);
    }
}
