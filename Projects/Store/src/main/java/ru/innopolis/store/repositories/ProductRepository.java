package ru.innopolis.store.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.innopolis.store.model.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {
}
