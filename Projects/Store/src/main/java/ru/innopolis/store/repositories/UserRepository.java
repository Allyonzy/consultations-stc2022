package ru.innopolis.store.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.innopolis.store.model.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {

    /**
     * Поиск пользователя по электронной почте
     * @param email - адрес электронной почты
     * @return пользователя
     */
    Optional<User> findByEmail(String email);
}
