package ru.innopolis.store.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name="store_order")
public class Order {
    /**
     * Идентификатор заказа
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * дата заказа
     */
    private LocalDate orderDate;

    /**
     * количество товаров
     */
    private Integer count;

    /**
     * id-товара (внешний ключ)
     */
    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    /**
     * id-заказчика (внешний ключ)
     */
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private User customer;

}
