package ru.innopolis.store.services;


import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.innopolis.store.model.Product;
import ru.innopolis.store.repositories.ProductRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();

    }

    @Override
    public Product findProductById(Integer id) {
        return productRepository.findById(id).orElseThrow(RuntimeException::new);
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        productRepository.deleteById(id);
    }

    @Override
    public void update(Integer id, Product productForm) {
        Product product = Product.builder()
                .id(id)
                .name(productForm.getName())
                .price(productForm.getPrice())
                .amount(productForm.getAmount())
                .build();

        productRepository.save(product);
    }

}
