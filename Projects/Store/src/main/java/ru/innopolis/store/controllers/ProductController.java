package ru.innopolis.store.controllers;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.innopolis.store.model.Product;
import ru.innopolis.store.services.ProductService;

import java.util.List;

@Controller
@AllArgsConstructor
@RequestMapping("/product")
public class ProductController {

    private final ProductService productService;

    /**
     * Посмотреть все товары
     * @param model - модель, отображаемая в шаблоне
     * @return - возвращаемся на страницу, model передается как строка
     */
    @GetMapping
    public String getProductPage(Model model) {
        return "product";
    }

    @GetMapping("/{product-id}")
    public String getRefreshedProduct(Model model, @PathVariable("product-id") Integer productId) {
        Product product = productService.findProductById(productId);
        model.addAttribute("product", product);
        return "product";
    }

    @PostMapping("/{product-id}/delete")
    public String deleteProduct(@PathVariable("product-id") Integer productId) {
        productService.delete(productId);
        return "redirect:/product";
    }

    @PostMapping("/{product-id}/update")
    public String updateProduct(@PathVariable("product-id") Integer productId, Product productForm) {
        productService.update(productId, productForm);
        return "redirect:/product";
    }

}

