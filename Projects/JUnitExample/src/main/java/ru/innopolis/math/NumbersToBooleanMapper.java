package ru.innopolis.math;

/**
 * junit-example
 */
@FunctionalInterface
public interface NumbersToBooleanMapper {
    boolean map(int value);
}
