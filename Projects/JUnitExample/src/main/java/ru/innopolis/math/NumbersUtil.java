package ru.innopolis.math;

/**
 * junit-example
 * тестовый вариант
 */
public class NumbersUtil implements NumbersToBooleanMapper {
    /**
     * Вычисляет простое число (проверка на простоту)
     * @param number
     * @return - true, false
     */
    public boolean isPrime(int number) {
        if (number == 0 || number == 1) {
            throw new IncorrectNumberException();
        }

        if (number == 2 || number == 3) {
            return true;
        }

        for (int i = 2; i * i <= number; i++) {
            if (number % i == 0) {
                return false;
            }
        }

        return true;
    }

    /**
     * Наибольший общий делитель
     * @param a
     * @param b
     * @return - число НОД для a и b
     */
    public int gcd(int a, int b) {
        while (b != 0) {
            int tmp = a % b;
            a = b;
            b = tmp;
        }
        return a;
    }

    /**
     * Преобразователь для вывода isPrime
     * @param value
     * @return
     */
    @Override
    public boolean map(int value) {
        return isPrime(value);
    }
}
