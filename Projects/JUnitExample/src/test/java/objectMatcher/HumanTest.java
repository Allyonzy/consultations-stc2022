package objectMatcher;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.hamcrest.Matchers.hasProperty;

//обрабатывает по-другому нижние подчеркиваания
//@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
//@DisplayName("NumbersUtil is working when")
public class HumanTest {

    @Test
    public void givenObject_whenHasValue_thenCorrect() {
        Human human = new Human("Petrov", "1234 768765");
        assertThat(human, hasProperty("name"));
    }

    @Test
    public void givenObject_whenHasCorrectValue_thenCorrect() {
        Human human = new Human("Petrov", "1234 768765");
        assertThat(human, hasProperty("passport", equalToIgnoringCase("1234 768765")));
    }
}
