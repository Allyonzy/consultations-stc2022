package ru.innopolis.repository;

import ru.innopolis.jpa.User;

import java.util.List;

/**
 * 15.11.2021
 * 20. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface UsersRepository {
    /**
     * Поиск всех пользователей
     * @return - список пользователей
     */
    List<User> findAll();

    /**
     * Поиск в базе по имени
     * @param name - имя
     * @return - список с заданным именем
     */
    List<User> findByName(String name);

    /**
     * Сохранение пользователя в базе данных
     * @param user - измененный пользователь
     */
    void save(User user);
}
