-- создание таблицы
create table if not exists account
(
    id         serial primary key, -- идентификатор строки, всегда уникальный, генерируется базой данных
    -- определяем столбцы
    first_name varchar(20),        -- строка максимальной длины 20
    last_name  varchar(20),
    age        integer check (age > 0 and age < 120)
);

-- добавление информации
insert into account(first_name, last_name, age)
values
    ('Савелий', 'Крамеров', 45),
    ('Артём', 'Иванов', 28),
    ('Денис', 'Поляков', 34),
    ('Вадик', 'Ростропович', 23),
    ('Марина', 'Носова', 43),
    ('Кит', 'Котов', 12),
    ('Роксана', 'Доброжилова', 68);
