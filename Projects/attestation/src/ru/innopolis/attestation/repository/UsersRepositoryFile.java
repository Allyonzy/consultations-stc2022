package ru.innopolis.attestation.repository;

import ru.innopolis.attestation.model.User;

/**
 * Интерфейс для работы с файлом пользователей
 *
 */
public interface UsersRepositoryFile {
    //List<User> findAll();

    /**
     * Поиск пользователя по ID
     * @param id - идентификатор пользователя
     * @return - пользователя с заданнм id
     */
    User findById(int id); //поиск юзера по ID

    /**
     * Cоздание нового юзера (запись в файл)
     * @param user
     */
    void create(User user);

    /**
     * Обновление информации по пользователю
     * @param user
     */
    void update(User user);

    /**
     * Удаление юзера по ID
     * @param id
     */
    void delete(int id);
}
