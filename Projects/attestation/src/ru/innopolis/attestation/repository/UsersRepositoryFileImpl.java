package ru.innopolis.attestation.repository;

import ru.innopolis.attestation.model.User;

import java.util.ArrayList;
import java.util.List;

public class UsersRepositoryFileImpl implements UsersRepositoryFile {

    private String fileName;

    //вынести в поля имплементации findAll()

    public UsersRepositoryFileImpl(String fileName) { this.fileName =  fileName;}

    @Override
    public User findById(int id) {
        //ищет данные в файле по идентификатору
        //findAll() + поиск по списку
        //Ошибка пользователь с заданным id не найден

        //Optional
        return null; //возвращает пользователя
    }

    @Override
    public void create(User user) {
        //добавление нового пользотеля
        //прочитать файл и в конец записать строку

        //findAll() + 1 User
    }

    @Override
    public void update(User user) {
        //изменение пользователя
        //findAll()
        //меняем по id
        //перезапись файла
    }

    @Override
    public void delete(int id) {
        //findAll()
        //поиск по id
        //удаление из коллекции
        //перезапись файла без указанного пользователя
    }

    // Один из варинтов
    // Вариант 1
    // Варинт 2 - HashMap по желанию
    public List<User> findAll(){
        //реализация метода
        //читает файл и преобразовывает данные в список
        return new ArrayList<>(); //возвращает полный список пользователей
    }

}
