package ru.innopolis.attestation.app;

import ru.innopolis.attestation.repository.UsersRepositoryFile;
import ru.innopolis.attestation.repository.UsersRepositoryFileImpl;

public class Main {
    public static void main(String[] args) {
        // вызов репозитория (создание объекта Impl)
        // параметры по желанию
        // обработать исключения - файл не читается, файл отсутствует
        UsersRepositoryFile usersRepositoryFile = new UsersRepositoryFileImpl("ru/innopolis/attestation/data/users.txt");

        //**findAll поможет выгрузить все данные из файл**

        //*** Сделать 3 раза ***
        //проверка findById(id)
        //преобразовать объект
        //проверка update(параметры update) - заменяет информацию в файле
        //распечатка в консоль, что получилось

        //*** Сделать 3 раза ***
        //проверка метода create (скорее всего последний id + 1)
        //распечатка в консоль, что получилось

        //проверка метода удаления
        //распечатка в консоль, что получилось

        System.exit(0);

    }
}