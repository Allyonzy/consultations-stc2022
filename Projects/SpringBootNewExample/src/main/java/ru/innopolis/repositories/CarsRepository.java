package ru.innopolis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.innopolis.models.Car;

import java.util.List;

/**
 * Репозиторий для работы с машинами
 */
public interface CarsRepository extends JpaRepository<Car, Integer> {
    List<Car> findAllByOwner_Id(Integer id);
    List<Car> findAllByOwnerIsNull();

    List<Car> findAllByColorAndModel(String color, String model);
}
