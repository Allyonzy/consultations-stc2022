package ru.innopolis.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.innopolis.forms.SignUpForm;
import ru.innopolis.services.SignUpService;

/**
 * Страница регистрации пользователя
 */
@RequiredArgsConstructor
@Controller
@RequestMapping("/sign-up")
public class SignUpController {

    private final SignUpService signUpService;

    @GetMapping
    public String getSignUpPage() {
        return "sign-up";
    }

    /**
     * Сохрание пользователя из формы регистрации
     * @param form
     * @return
     */
    @PostMapping
    public String signUpUser(SignUpForm form) {
        signUpService.saveUserData(form);
        return "redirect:/sign-in";
    }

}
