package ru.innopolis.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.innopolis.forms.UserForm;
import ru.innopolis.models.Car;
import ru.innopolis.models.User;
import ru.innopolis.services.UsersService;

import javax.validation.Valid;
import java.util.List;

/**
 * Страница работы со списком пользователей
 */
@Controller
@AllArgsConstructor
public class UsersController {
    private static final String COMMON_REDIRECT_URL = "redirect:/users";

    private final UsersService usersService;

    @GetMapping("/users")
    public String getUsersPage(Model model) {
        List<User> users = usersService.findAll();
        model.addAttribute("users", users);
        return "users";
    }

    @GetMapping("/users/{user-id}")
    public String getUserPage(Model model, @PathVariable("user-id") Integer userId) {
        try {
            User user = usersService.findUserById(userId);
            model.addAttribute("user", user);

        } catch (RuntimeException e) {
            System.err.println(e);
        }
        return "user";
    }
    @PostMapping("/users")
    public String addUser(
            @Valid UserForm form,
            BindingResult result,
            RedirectAttributes forRedirectModel
    ) {
        if (result.hasErrors()) {
                forRedirectModel.addFlashAttribute("errors", "Форма заполнена с ошибками!");
                return COMMON_REDIRECT_URL;
        }
        usersService.addUser(form);
        return COMMON_REDIRECT_URL;
    }

    @PostMapping("/users/{user-id}/delete")
    public String deleteUser(@PathVariable("user-id") Integer userId) {
        usersService.deleteUserById(userId);
        return COMMON_REDIRECT_URL;
    }

    @PostMapping("/users/{user-id}/update")
    public String update(@PathVariable("user-id") Integer userId, UserForm userForm) {
        usersService.updateUserProperties(userId, userForm);
        return COMMON_REDIRECT_URL;
    }

    @GetMapping("/users/{user-id}/cars")
    public String getCarsByUser(Model model, @PathVariable("user-id") Integer userId) {
        List<Car> cars = usersService.findCarsByUserId(userId);
        List<Car> newCars = usersService.getCarsWithoutOwner();
        model.addAttribute("userId", userId);
        model.addAttribute("cars", cars);
        model.addAttribute("newCars", newCars);
        return "cars-of-user";
    }

    @PostMapping("/users/{user-id}/cars")
    public String addCarToUser(
            @PathVariable("user-id") Integer userId,
            @RequestParam("car-id") Integer carId
    ) {
        usersService.addCarToUser(userId, carId);
        String[] urlToStr = new String[] {COMMON_REDIRECT_URL, String.valueOf(userId), "cars" };
        return String.join("/", urlToStr);
    }


}
