package ru.innopolis.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Страница логирования
 */
@Controller
@RequestMapping("/sign-in")
public class SignInController {
    @GetMapping
    public String getSignInPage() {
        return "sign-in";
    }
}
