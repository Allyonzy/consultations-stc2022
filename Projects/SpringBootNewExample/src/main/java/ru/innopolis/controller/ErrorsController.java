package ru.innopolis.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Страница 404
 */
@Controller
@RequestMapping("/error/404")
public class ErrorsController {

    @GetMapping()
    public String get404Page() {
        return "errors/error-404";
    }
}
