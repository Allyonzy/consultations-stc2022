package ru.innopolis.models;

/**
 * Роли для аутентификации в проекте
 */
public enum Role {
    ADMIN,
    USER
}
