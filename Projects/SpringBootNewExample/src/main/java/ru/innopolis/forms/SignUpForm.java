package ru.innopolis.forms;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * Форма регистрации пользователя
 */
@Data
public class SignUpForm {
    @NotEmpty
    private String firstName;
    private String lastName;

    @NotEmpty
    private String email;

    @NotEmpty
    private String password;
}
