package ru.innopolis.services;

import ru.innopolis.forms.UserForm;
import ru.innopolis.models.Car;
import ru.innopolis.models.User;

import java.util.List;

public interface UsersService {
    /**
     * Добавить нового пользователя
     * @param form - форма заполнения
     */
    void addUser(UserForm form);

    /**
     * Найти всех пользователей
     * @return - список пользователей
     */
    List<User> findAll();

    /**
     * Удалить пользователя по идентификатору
     * @param userId - идентификатор пользователя
     */
    void deleteUserById(Integer userId);

    /**
     * Получить пользователя по идентификатору
     * @param userId - идентификатор пользователя
     * @return пользователь
     */
    User findUserById(Integer userId);

    /**
     * Получить машину по идентификатору пользователя
     * @param userId - идентификатор пользователя
     * @return список машин
     */
    List<Car> findCarsByUserId(Integer userId);

    /**
     * Получить новую машину без владельца
     * @return список машин
     */
    List<Car> getCarsWithoutOwner();

    /**
     * Добавить машину
     * @param userId - идентификатор пользователя
     * @param carId - идентификатор машины
     */
    void addCarToUser(Integer userId, Integer carId);

    /**
     * Обновить данные пользователя (имя, фамилия)
     * @param userId - идентификатор пользователя
     * @param userForm - данные формы
     */
    void updateUserProperties(Integer userId, UserForm userForm);
}
