package ru.innopolis.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.innopolis.exceptions.UserNotFoundException;
import ru.innopolis.forms.UserForm;
import ru.innopolis.models.Car;
import ru.innopolis.models.User;
import ru.innopolis.repositories.CarsRepository;
import ru.innopolis.repositories.UsersRepository;

import java.util.List;

@RequiredArgsConstructor
@Component
public class UsersServiceImpl implements UsersService {
    private final UsersRepository usersRepository;
    private final CarsRepository carsRepository;

    @Override
    public void addUser(UserForm form) {
        User user = User.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .build();

        usersRepository.save(user);
    }

    @Override
    public List<User> findAll() {
        return usersRepository.findAll();
    }

    @Override
    public void deleteUserById(Integer userId) {
        usersRepository.deleteById(userId);
    }

    @Override
    public User findUserById(Integer userId) {
        return usersRepository.findById(userId).orElseThrow(UserNotFoundException::new);
    }

    @Override
    public List<Car> findCarsByUserId(Integer userId) {
        return carsRepository.findAllByOwner_Id(userId);
    }

    @Override
    public List<Car> getCarsWithoutOwner() {
        return carsRepository.findAllByOwnerIsNull();
    }

    @Override
    public void addCarToUser(Integer userId, Integer carId) {
        User user = usersRepository.getById(userId);
        Car car = carsRepository.getById(carId);
        car.setOwner(user);
        carsRepository.save(car);
    }

    @Override
    public void updateUserProperties(Integer userId, UserForm userForm) {
        User user = usersRepository.getById(userId);
        user.setFirstName(userForm.getFirstName());
        user.setLastName(userForm.getLastName());
        usersRepository.save(user);
    }
}
