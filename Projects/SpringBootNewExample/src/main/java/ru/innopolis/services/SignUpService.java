package ru.innopolis.services;

import ru.innopolis.forms.SignUpForm;

public interface SignUpService {
    /**
     * Сохранение данных пользователя
     * @param form - форма заполнения
     */
    void saveUserData(SignUpForm form);
}
