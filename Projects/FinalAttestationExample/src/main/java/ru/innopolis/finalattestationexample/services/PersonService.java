package ru.innopolis.finalattestationexample.services;

import ru.innopolis.finalattestationexample.model.Person;

import java.util.List;

public interface PersonService {
    /**
     *
     * @return
     */
    List<Person> findAll();

    /**
     *
     * @param id
     * @return
     */
    Person findById(Integer id);

    /**
     *
     * @param person
     */
    void create(Person person);

    /**
     *
     * @param person
     */
    void update(Person person);

    /**
     *
     * @param id
     */
    void delete(int id);
}
