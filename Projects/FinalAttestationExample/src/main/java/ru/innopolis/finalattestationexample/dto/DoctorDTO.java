package ru.innopolis.finalattestationexample.dto;

public class DoctorDTO {

    private String doctorFullName;

    public DoctorDTO() {

    }

    public DoctorDTO(String doctorFullName) {
        this.doctorFullName = doctorFullName;
    }


    public String getDoctorFullName() {
        return doctorFullName;
    }

    public void setDoctorFullName(String doctorFullName) {
        this.doctorFullName = doctorFullName;
    }
}
