package ru.innopolis.finalattestationexample.dto;

public class PatientDTO {

    private String patientFullName;

    public PatientDTO() {

    }

    public PatientDTO(String patientFullName) {
        this.patientFullName = patientFullName;
    }


    public String getPatientFullName() {
        return patientFullName;
    }

    public void setPatientFullName(String patientFullName) {
        this.patientFullName = patientFullName;
    }
}
