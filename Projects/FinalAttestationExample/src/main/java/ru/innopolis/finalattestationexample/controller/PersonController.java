package ru.innopolis.finalattestationexample.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.innopolis.finalattestationexample.model.Person;
import ru.innopolis.finalattestationexample.services.PersonService;

@Controller
@AllArgsConstructor
@RequestMapping("/person")
public class PersonController {
    private final PersonService personService;

    @GetMapping("/{person-id}")
    public String getProfilePage(Model model, @PathVariable("person-id") Integer personId) {
        Person person = personService.findById(personId);
        model.addAttribute("person", person);
        return "profile-page";
    }
}
