package ru.innopolis.finalattestationexample.services;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.innopolis.finalattestationexample.exception.NotFoundException;
import ru.innopolis.finalattestationexample.model.Person;
import ru.innopolis.finalattestationexample.repository.PersonRepository;

import java.util.List;

@Service
@AllArgsConstructor
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository; //переменная обязательна

//    public PersonServiceImpl(PersonRepository personRepository) {
//        this.personRepository = personRepository;
//    }

    @Override
    public List<Person> findAll() {
        return null;
    }

    @Override
    public Person findById(Integer id) {
        return personRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    @Override
    public void create(Person person) {

    }

    @Override
    public void update(Person person) {

    }

    @Override
    public void delete(int id) {

    }
}
