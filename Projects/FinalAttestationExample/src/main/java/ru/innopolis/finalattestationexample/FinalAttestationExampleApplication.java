package ru.innopolis.finalattestationexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinalAttestationExampleApplication {

	public static void main(String[] args) {
        SpringApplication.run(FinalAttestationExampleApplication.class, args);
	}

}
