package ru.innopolis.finalattestationexample.controller;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.innopolis.finalattestationexample.model.RegistrationBilet;
import ru.innopolis.finalattestationexample.services.RegistrationBiletService;

import java.util.List;

@Controller
@RequestMapping("/registration")
@AllArgsConstructor
public class RegistrationBiletController {

    public final RegistrationBiletService registrationBiletService;

    public String registrationBiletList(Model model) {
        List<RegistrationBilet> registrationBiletList = registrationBiletService.findAll();
        model.addAttribute("registrationBiletList", registrationBiletList);
        return "main_registration";
    }

    /**
     * @PathVariable
     *
     * localhost:8082/registration/{registration-date}
     * localhost:8082/registration/2022-08-03
     * @param model
     * @return
     */
    @GetMapping("/{registration-date}")
    public String registrationBiletListByDate(Model model, @PathVariable("registration-date") String registrationDateString) {
        //TODO преобразовать дату в LocalDate

        return "redirect: /registration/";
    }

}
