package ru.innopolis.finalattestationexample.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "person")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id; //UUID
    private String firstName;
    private String middleName;
    private String lastName;
    private LocalDate lastVisitDate;

    private Role role;
    //дописать

//    @OneToMany(mappedBy = "doctor", cascade = {CascadeType.REMOVE})
//    List<RegistrationBilet> registrationPatientList;
//
//    @OneToMany(mappedBy = "patient", cascade = {CascadeType.REMOVE})
//    List<RegistrationBilet> registrationDoctorList;

    public String getPersonFullName() {
        return firstName + " " + middleName + " " + lastName;
    }

}
