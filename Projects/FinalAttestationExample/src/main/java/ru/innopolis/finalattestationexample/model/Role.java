package ru.innopolis.finalattestationexample.model;

public enum Role {
    DOCTOR,
    PATIENT
}
