package ru.innopolis.finalattestationexample.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "registration_info")
public class RegistrationBilet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name="doctor_id")
    private Person doctor;

    @ManyToOne
    @JoinColumn(name="patient_id")
    private Person patient;

    private Integer biletNumber;
    private LocalDate registratioDate;

}
