package ru.innopolis.finalattestationexample.services;

import ru.innopolis.finalattestationexample.model.Person;
import ru.innopolis.finalattestationexample.model.RegistrationBilet;

import java.util.List;

public interface RegistrationBiletService {

    /**
     * Получить список всех регистраций в приложении
     */
    /**
     * 
     * @return
     */
    List<RegistrationBilet> findAll();


    /**
     * Получить регистрацию по пользователю
     *
     * @param person - пациент
     * @return - список регистраций по пациента
     */
    List<RegistrationBilet> findByPatient(Person person);

    /**
     * Получить регистрацию по идентификатору пользователя
     *
     * @param personId - идентификатор пациента
     * @return - список регистраций по пациента
     */
    List<RegistrationBilet> findByPatientId(Integer personId);

    /**
     *
     * @param registrationBilet
     */
    void create(RegistrationBilet registrationBilet);

    /**
     *
     * @param registrationBilet
     */
    void update(RegistrationBilet registrationBilet);

    /**
     *
     * @param id
     */
    void delete(int id);
}
