package ru.innopolis.finalattestationexample.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.innopolis.finalattestationexample.model.Person;
import ru.innopolis.finalattestationexample.model.RegistrationBilet;
import ru.innopolis.finalattestationexample.repository.PersonRepository;
import ru.innopolis.finalattestationexample.repository.RegistrationBiletRepository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class RegistrationBiletServiceImpl implements RegistrationBiletService {

    public final RegistrationBiletRepository registrationBiletRepository;

    public final PersonRepository personRepository;

    @Override
    public List<RegistrationBilet> findAll() {
        return registrationBiletRepository.findAll();
    }

    @Override
    public List<RegistrationBilet> findByPatient(Person person) {
        return registrationBiletRepository.findByPatientOrderById(person);
    }

    @Override
    public List<RegistrationBilet> findByPatientId(Integer personId) {
        Optional<Person> person = personRepository.findById(personId);

        List<RegistrationBilet> registrationBiletList = new ArrayList<>();

        if(person.isPresent()) {
            registrationBiletList = registrationBiletRepository.findByPatientOrderById(person.get());
        }
        //TODO обработать ошибку не найденного пользователя

        return registrationBiletList;
    }

    @Override
    public void create(RegistrationBilet registrationBilet) {

    }

    @Override
    public void update(RegistrationBilet registrationBilet) {

    }

    @Override
    public void delete(int id) {

    }
}
