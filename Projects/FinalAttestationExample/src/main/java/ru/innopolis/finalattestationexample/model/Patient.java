package ru.innopolis.finalattestationexample.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "patient")
public class Patient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id; //UUID

    @OneToOne
    private Person personalData;

    @OneToMany(mappedBy = "patient", cascade = {CascadeType.REMOVE})
    private List<RegistrationBilet> registrationPatientList;

    //добавим поля, специфические для пациента
}
