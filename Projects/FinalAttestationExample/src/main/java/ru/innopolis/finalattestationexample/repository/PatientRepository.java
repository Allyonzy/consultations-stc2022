package ru.innopolis.finalattestationexample.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.innopolis.finalattestationexample.model.Doctor;
import ru.innopolis.finalattestationexample.model.Patient;

public interface PatientRepository extends JpaRepository<Patient, Integer>  {
}
