package ru.innopolis.finalattestationexample.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.innopolis.finalattestationexample.model.Person;

import java.util.List;

public interface PersonRepository extends JpaRepository<Person, Integer> {
    List<Person> findPersonByFirstName(String firstName);
    void deletePersonById(Integer personId);
}
