package ru.innopolis.finalattestationexample.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.innopolis.finalattestationexample.model.Doctor;

public interface DoctorRepository extends JpaRepository<Doctor, Integer> {
}
