package ru.innopolis.finalattestationexample.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "doctor")
public class Doctor {

    //дописать
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id; //UUID

    @OneToOne
    private Person personalData;

    @OneToMany(mappedBy = "doctor", cascade = {CascadeType.REMOVE})
    private List<RegistrationBilet> registrationPatientList;

    //добавим поля, специфические для доктора
}
