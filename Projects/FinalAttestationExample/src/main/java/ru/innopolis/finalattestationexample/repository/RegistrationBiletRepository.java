package ru.innopolis.finalattestationexample.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.innopolis.finalattestationexample.model.Person;
import ru.innopolis.finalattestationexample.model.RegistrationBilet;

import java.util.List;

public interface RegistrationBiletRepository extends JpaRepository<RegistrationBilet, Integer> {


    List<RegistrationBilet> findByPatientOrderById(Person patient);
}
