package figure;

public class Rectangle extends Figure {
    protected int a;
    private int b;

    public Rectangle(int x, int y, int a, int b) {
        super(x, y);
        if (a > 0 && b > 0) {
            this.a = a;
            this.b = b;
        }
    }

    @Override
    public int getPerimeter() {
        return 2 * (a + b);
    }
}
