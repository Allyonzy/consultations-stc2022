package figure;

import move.Movable;

public class Circle extends Ellipse implements Movable {

    public Circle(int x, int y, int a) {
        super(x, y, a, a);
    }

    /**
     * Перемещаем на заданные координаты
     * Вариант 2 - прибавление координат
     * @param kX координата x для перемещения
     * @param kY координата y для перемещения
     */
    @Override
    public void move(int kX, int kY) {
        System.out.print("Начальные координаты круга: (" + this.x + "; " + this.y + ").");
        this.x = kX;
        this.y = kY;
        System.out.println(" Круг был перемещен в заданную точку. Новые координаты круга: (" + this.x + "; " + this.y + ").");
    }
}
