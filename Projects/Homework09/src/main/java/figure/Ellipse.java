package figure;

public class Ellipse extends Figure {
    protected int a;
    private int b;

    public Ellipse(int x, int y, int a, int b) {
        super(x, y);
        if (a > 0 && b > 0) {
            this.a = a;
            this.b = b;
        }
    }

    @Override
    public int getPerimeter() {
        double perimeter = 4 * (Math.PI * a * b + (a - b) * (a - b)) / (a + b);
        return (int) perimeter;
    }
}
