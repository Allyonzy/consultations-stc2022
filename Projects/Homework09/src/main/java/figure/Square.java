package figure;

import move.Movable;

public class Square extends Rectangle implements Movable {

    public Square(int x, int y, int a) {
        super(x, y, a, a);
    }

    @Override
    public void move(int kX, int kY) {
        System.out.print("Начальные координаты квадрата: (" + this.x + "; " + this.y + ").");
        this.x = kX;
        this.y = kY;
        System.out.println(" Квадрат был перемещен в заданную точку. Новые координаты квадрата: (" + this.x + "; " + this.y + ").");
    }
}
