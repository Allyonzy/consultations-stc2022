package move;

public interface Movable {
    void move(int kX, int kY);
}
