import figure.*;
import move.Movable;

public class Main {
    public static void main(String[] args) {
        int figureSize = 4; // генерация рандомного числа фигур - дополнение
        Figure [] figures = new Figure[figureSize];

        Figure ellipse = new Ellipse(4, 4, 2, 8); // генерация рандомного числа координат и размерности
        Figure rectangle = new Rectangle(2, 2, 4, 10);
        Figure circle = new Circle(3, 4, 6);
        Figure square = new Square(2, 2, 4);

        figures[0] = ellipse;
        figures[1] = rectangle;
        figures[2] = circle;
        figures[3] = square;

        // в цикле выводим периметр и передвигаем, если есть интерфейс
        for(Figure fig : figures) {
            System.out.println("Периметр фигуры: " + fig.getPerimeter());

            if(fig instanceof Movable) {
                ((Movable) fig).move(1, 1);
            }
        }
        System.out.println("************************************************************************************************************************");

        // вариант 2 отдельное передвижение
        int movableSize = 2;
        Movable[] movable = new Movable[movableSize];
        movable[0] = (Movable) square;
        movable[1] = (Movable) circle;

        for (Movable figureToMove : movable) {
            figureToMove.move(3, 6);
        }

        for (Movable figureToMove : movable) {
            figureToMove.move(5, 5);
        }
    }
}
