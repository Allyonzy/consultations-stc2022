package ru.innopolis.streamExample;

public enum Level {
    TRAINEE,
    JUNIOR,
    MIDDLE,
    SENIOR
}
