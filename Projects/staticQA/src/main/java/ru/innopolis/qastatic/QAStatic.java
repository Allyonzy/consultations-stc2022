package ru.innopolis.qastatic;

public class QAStatic {
    //Поле класса, переменная для экземпляра
    private String test = "Проверочная переменная";
    public static String testStatic = "Проверочная статическая переменная";

    //Поле класса, есть у всех классов, константа
    public static final String STRING_STATIC_NOTCHANGED_FIELD = "Константа";
    public static final int NUMBER_STATIC_NOTCHANGED_FIELD = 5;

    //constructor
    public QAStatic() {
        testStatic = "переменная в конструкторе";
        System.out.println("вызван конструктор");
    }



    //instance (экземпляр) initializer
    {
        test = "Поменяем значение переменной";
        System.out.println("вызван instance инициализатор экземпляра");
    }

    //static initializer
    static {
        testStatic = "Проверочная статическая переменная";
        System.out.println("вызван static инициализатор");
    }


    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }
}
