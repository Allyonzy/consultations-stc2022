package ru.innopolis.qastatic;

public class StaticMain {
    public static void main(String[] args) {
        QAStatic qaStaticFirst = new QAStatic();
        QAStatic qaStaticSecond = new QAStatic();
        String text = QAStatic.STRING_STATIC_NOTCHANGED_FIELD;
        int number = QAStatic.NUMBER_STATIC_NOTCHANGED_FIELD;
        String textStatic = QAStatic.testStatic;
        String test = qaStaticFirst.getTest();
    }
}
