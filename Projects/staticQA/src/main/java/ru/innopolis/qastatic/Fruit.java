package ru.innopolis.qastatic;

import java.util.Arrays;

public class Fruit {
    private static String[] fruitArray;

    static {
        System.out.println("Внутри Static инициализоватора.");

        // массив фруктов
        String[] fruits = new String[3];
        fruits[0] = "Яблоко";
        fruits[1] = "Груша";
        fruits[2] = "Абрикос";

        // print fruits
        for (String fruit : fruits) {
            System.out.println(fruit);
        }
        fruitArray = fruits;
        System.out.println("Закончить статический инициализатор.\n");
    }

    public static void main(String[] args) {
        System.out.println("Вошли в метод main.");
        System.out.println(Arrays.toString(fruitArray));
    }
}