import java.io.BufferedWriter;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {


// вот такая конструкция считается Идеей невалидной, т.к. содержит неперехваченные исключения

        //Чтение файла
        //Создать список пользователей из файла
        //Преобразовать в стрим


        //Открыть файл
        //Список в цикле записываем в файл toString()


        try {
            Stream<String> userStream = "<создание _стрима>";
            try (BufferedWriter br = new BufferedWriter(""))
            {
                userStream.forEach(line -> {
                    br.write(line);
                    br.newLine();
                });
            } catch (Exception ex) {
                //обработка
            }
        } catch (Exception ex) {
            //обработка
        }

    }
}