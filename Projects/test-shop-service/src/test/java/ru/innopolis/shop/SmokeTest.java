package ru.innopolis.shop;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.innopolis.shop.controllers.DiscountController;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.is;

@SpringBootTest
class SmokeTest {

    @Autowired
    private DiscountController discountController;

    @Test
    void contextLoads() {
        assertThat(discountController, is(notNullValue()));
    }

}
