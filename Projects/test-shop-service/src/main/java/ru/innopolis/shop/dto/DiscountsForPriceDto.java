package ru.innopolis.shop.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 
 * shop-service
 *
 * 
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DiscountsForPriceDto {
    private Double price;
    private List<DiscountForPriceDto> discounts;
}
