package ru.innopolis.shop.services;

import ru.innopolis.shop.dto.OrdersPricesDto;
import ru.innopolis.shop.dto.DiscountDto;
import ru.innopolis.shop.dto.DiscountsForPricesDto;

import java.util.List;

/**
 * 
 * shop-service
 *
 * 
 * @version v1.0
 */
public interface DiscountsService {
    List<DiscountDto> getDiscountsByType(String type);

    DiscountsForPricesDto applyDiscounts(OrdersPricesDto ordersPrices);
}
