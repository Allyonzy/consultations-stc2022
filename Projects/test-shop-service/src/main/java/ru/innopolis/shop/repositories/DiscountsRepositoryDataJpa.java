package ru.innopolis.shop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.innopolis.shop.models.Discount;

import java.util.List;

/**
 * 
 * shop-service
 *
 * 
 * @version v1.0
 */
public interface DiscountsRepositoryDataJpa extends JpaRepository<Discount, Long> {
    List<Discount> findAllByType(Discount.Type type);
}
