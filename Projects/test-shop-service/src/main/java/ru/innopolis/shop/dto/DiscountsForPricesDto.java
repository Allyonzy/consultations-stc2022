package ru.innopolis.shop.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 
 * shop-service
 *
 * 
 * @version v1.0
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DiscountsForPricesDto {
    private List<DiscountsForPriceDto> data;
}
