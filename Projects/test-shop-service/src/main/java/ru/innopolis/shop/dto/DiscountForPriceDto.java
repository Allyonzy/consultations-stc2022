package ru.innopolis.shop.dto;

import lombok.Builder;
import lombok.Data;

/**
 * 
 * shop-service
 *
 * 
 * @version v1.0
 */
@Data
@Builder
public class DiscountForPriceDto {
    private Double percents;
    private Double priceByDiscount;
}
