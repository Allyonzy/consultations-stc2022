package ru.innopolis.shop.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.innopolis.shop.dto.OrdersPricesDto;
import ru.innopolis.shop.models.Discount;
import ru.innopolis.shop.dto.DiscountDto;
import ru.innopolis.shop.dto.DiscountsForPricesDto;
import ru.innopolis.shop.exceptions.IncorrectPrice;
import ru.innopolis.shop.exceptions.IncorrectType;
import ru.innopolis.shop.repositories.DiscountsRepositoryDataJpa;
import ru.innopolis.shop.repositories.DiscountsRepositoryJdbc;

import java.util.List;

/**
 * 
 * shop-service
 *
 * 
 * @version v1.0
 */
@Service
public class DiscountsServiceImpl implements DiscountsService {

    @Autowired
    private DiscountsRepositoryDataJpa discountsRepository;

    @Autowired
    private DiscountsRepositoryJdbc discountsRepositoryJdbc;

    @Override
    public List<DiscountDto> getDiscountsByType(String type) {
        Discount.Type discountType;
        try {
            discountType = Discount.Type.valueOf(type);
        } catch (IllegalArgumentException e) {
            throw new IncorrectType();
        }
        return DiscountDto.from(discountsRepository.findAllByType(discountType));
    }

    @Override
    public DiscountsForPricesDto applyDiscounts(OrdersPricesDto ordersPrices) {
        if (ordersPrices.getPrices().stream().anyMatch(value -> value < 0)) {
            throw new IncorrectPrice("Price must be positive");
        }
        return discountsRepositoryJdbc.applyAllDiscounts(ordersPrices.getPrices());
    }
}
