package ru.innopolis.shop.repositories;

import ru.innopolis.shop.dto.DiscountsForPricesDto;

import java.util.List;

/**
 * 
 * shop-service
 *
 * 
 * @version v1.0
 */
public interface DiscountsRepositoryJdbc {
    DiscountsForPricesDto applyAllDiscounts(List<Double> prices);
}
