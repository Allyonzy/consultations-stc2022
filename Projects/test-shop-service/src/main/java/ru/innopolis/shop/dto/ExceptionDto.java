package ru.innopolis.shop.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 11.05.2021
 * shop-service
 *
 * 
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExceptionDto {
    private int code;
    private String message;
}
