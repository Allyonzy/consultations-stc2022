package TestIncapsulation;

import java.util.Random;
import java.util.Scanner;

public class Main {

    private static int RANDOM_MAX = 200;
    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        Random rnd = new Random();
        System.out.println("Введите количество ламп:");
        int lampArraySize = scn.nextByte();
        Lamp[] lamps = new Lamp[lampArraySize];
        for (int i = 0; i < lamps.length; i++) {
            System.out.println("Введите производителя лампы: ");
            String lampManufacturer = scn.next();
            System.out.println("Введите цвет лампы: ");
            String lampColor = scn.next();

            // Создаем файл или массив с n производителей, цветов,
            lamps[i] = new Lamp(lampManufacturer, lampColor, rnd.nextInt(RANDOM_MAX));
        }
        System.out.println("Исходный массив: ");
        for (Lamp lamp : lamps) {
            System.out.println(lamps.toString());
        }
        System.out.println("Сортировка по количеству:");
        Lamp[] sortedLamps = Lamp.sortByCount(lamps);
        for (Lamp someLamp : sortedLamps) {
            System.out.println(someLamp.toString());
        }
        scn.close();
    }


}
