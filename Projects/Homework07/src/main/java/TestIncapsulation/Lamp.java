package TestIncapsulation;

import java.util.Arrays;

public class Lamp {
    private String manufacturer;
    private String color;
    private int count;

    public Lamp() {}

    public Lamp(String manufacturer, String color, int count) {
        this.manufacturer = manufacturer;
        this.color = color;
        this.count = count;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    /**
     * Класс - ссылочный объект
     * @param lamps массив ламп
     */
    public static Lamp[] sortByCount(Lamp[] lamps) {
        int lampArraySize = lamps.length;
        Lamp[] arrayToSort = new Lamp[lampArraySize];
        System.arraycopy(lamps, 0, arrayToSort, 0, lampArraySize);
        int minIndex = 0;
        Lamp tempLamp;

        for (int i = 0; i < (lampArraySize - 1); i++) {
            minIndex = i;
            for (int j = (i + 1); j < lampArraySize; j++) {
                if (arrayToSort[minIndex].getCount() > arrayToSort[j].getCount()) {
                    minIndex = j;
                }
            }
            tempLamp = arrayToSort[i];
            arrayToSort[i] = arrayToSort[minIndex];
            arrayToSort[minIndex] = tempLamp;
        }
        return arrayToSort;
    }

    @Override
    public String toString() {
        return "Лампа " + "Производитель: " + manufacturer + "| Цвет: " + color +" | Количество на складе: " + count;
    }

}
