create table web_sales.customer
(
  id    serial primary key,
  fio   varchar(255),
  email   varchar(50) unique not null,
  time_sign_in timestamp
);

create table web_sales.product
(
    id    serial primary key,
    description   varchar(255),
    price   decimal,
    quantity integer
);

create table web_sales.order_list
(
    id    serial primary key,
    time_order timestamp,
    amount   integer,
    product_id integer,
    foreign key (product_id) references web_sales.product(id)
);

create table web_sales.customer_basket
(
    id    serial primary key,
    customer_id integer,
    order_list_id   integer,
    foreign key (customer_id) references web_sales.customer(id),
    foreign key (order_list_id) references web_sales.order_list(id)
);

--работа с базой данных--

--добавление данных в БД--
insert into web_sales.product(description, price, quantity)
values ('линолеум, 1м', 200, 1000);

insert into web_sales.product(description, price, quantity)
values ('цемент, 1кг', 100, 500);

insert into web_sales.product(description, price, quantity)
values ('плитка фигурная, 1шт', 250, 300);

insert into web_sales.customer(fio, email)
values ('Alan Rickman', 'a.rickman@gmail.com');

insert into web_sales.order_list(time_order, amount, product_id)
values ('2022-08-26 18:06:56.328337', 20, 2);

insert into web_sales.customer_basket(customer_id, order_list_id)
values (1, 1);

--выборка данных, выборка данных по параметру--

--обновление данных--

--запрос на удаление--