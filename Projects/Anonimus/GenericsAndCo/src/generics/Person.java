package generics;

import java.util.function.Function;

public class Person<T> {
    private final static int DEFAULT_SIZE = 10;
    private T personType;

    @SuppressWarnings("unchecked")
    public T[] sortPerson(T[] personsToSort, Function<T,T> funcOfSort) {
        int personSize = personsToSort.length;
        T[] personSortResult = (T[]) new Object [personSize]; //cast to T, преобразовать в T
        int currentIndex = 0;

        for (T person : personsToSort) {
            personSortResult[currentIndex] = person;
        }

        for (T personResult : personSortResult) {
            funcOfSort.apply(personResult);
        }
        return personSortResult;
    }
}
