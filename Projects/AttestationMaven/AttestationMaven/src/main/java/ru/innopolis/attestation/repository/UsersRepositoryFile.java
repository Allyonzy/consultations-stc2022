package ru.innopolis.attestation.repository;

import ru.innopolis.attestation.model.User;

/**
 * Интерфейс для работы с внешними данными
 *
 * API - тоже интерфейс
 */
public interface UsersRepositoryFile {
    //List<User> findAll();
    User findById(int id); //поиск юзера по ID
    void create(User user); //создание нового юзера (запись в файл)
    void update(User user); //обновление информации по юзеру
    void delete(int id); //удаление юзера по ID
}
