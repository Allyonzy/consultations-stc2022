package ru.innopolis.attestation.model;

public class User {

    private int id;
    private String name;
    private String surname;
    private int age;
    private boolean hasWork;

    public User(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setHasWork(boolean hasWork) {
        this.hasWork = hasWork;
    }

    public int getId() {
        return id;
    }

    public User(int id, String name, String surname, int age, boolean hasWork) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.hasWork = hasWork;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", hasWork=" + hasWork +
                '}';
    }

}
