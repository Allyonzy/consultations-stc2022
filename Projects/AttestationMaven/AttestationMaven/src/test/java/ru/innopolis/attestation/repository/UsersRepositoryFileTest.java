package ru.innopolis.attestation.repository;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.innopolis.attestation.model.User;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName(value = "UsersRepositoryTest is working when")
public class UsersRepositoryFileTest {


    private UsersRepositoryFileImpl usersRepositoryFile = new UsersRepositoryFileImpl("users_test_01.txt");
//    private UsersRepositoryFileImpl usersRepositoryFile = new UsersRepositoryFileImpl("users_test_02.txt");
//    private UsersRepositoryFileImpl usersRepositoryFile = new UsersRepositoryFileImpl("users_test_03.txt");

    private final Random rnd = new Random();

    @Test
    public void find_by_id_user_test() {

    }

    @Test
    public void create_user_test() {
        User user = new User(1, "Имя1", "Фамилия1", 10, false);
        usersRepositoryFile.create(user);
        User resultUser = usersRepositoryFile.findById(user.getId());
        Assertions.assertEquals(user, resultUser);
    }

    @Test
    public void update_user_test() {


        User testedUser = usersRepositoryFile.findById(rnd.nextInt(2));
        testedUser.setName("Ваня");
        testedUser.setSurname("Жуковский");
        testedUser.setAge(16);
        testedUser.setHasWork(true);

        usersRepositoryFile.update(testedUser);

        User resultUser = usersRepositoryFile.findById(testedUser.getId());
        Assertions.assertEquals(testedUser, resultUser);
    }

    @Test
    public void delete_user_test() {
        User testedUser = usersRepositoryFile.findById(rnd.nextInt(2));

        usersRepositoryFile.delete(testedUser.getId());

        User resultUser = usersRepositoryFile.findById(testedUser.getId());
        Assertions.assertNull(resultUser); //проверить отсутствие пользователя в файле
        assertThrows(NullPointerException.class, () -> {"Пользователь не найден";})//проверка исключения
    }


}
