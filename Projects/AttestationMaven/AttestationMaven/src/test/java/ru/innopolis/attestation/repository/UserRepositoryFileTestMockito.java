package ru.innopolis.attestation.repository;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
class UserRepositoryFileImplTestMockito {

    @Mock
    private TextFileDataBaseDriver driverMock;

    private UserRepositoryFileImpl userRepositoryFile;

    @BeforeEach
    void init() {
        userRepositoryFile = new UserRepositoryFileImpl("unit_test_user_db.txt", driverMock);
    }

    @Nested
    @DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
    @DisplayName("Method findAll()")
    class MethodFindAll {

        private List<UserEntity> users;

        @Nested
        @DisplayName("should be successful")
        class ShouldBeSuccessful {

            @BeforeEach
            void init() {
                users = new ArrayList<>();
            }

            @Test
            void when_no_user_exists() {
                doReturn(users).when(driverMock).readFromFile();

                assertEquals(users, userRepositoryFile.findAll());
            }

            @Test
            void when_only_one_user_exists() {
                users.add(new UserEntity(1, "Имя1", "Фамилия1", 10, false));

                doReturn(users).when(driverMock).readFromFile();

                assertEquals(users, userRepositoryFile.findAll());
            }

            @Test
            void when_several_users_exist() {;
                users.add(new UserEntity(1, "Имя1", "Фамилия1", 10, false));
                users.add(new UserEntity(2, "Имя2", "Фамилия2", 20, false));
                users.add(new UserEntity(5, "Имя5", "Фамилия5", 50, true));

                doReturn(users).when(driverMock).readFromFile();

                assertEquals(users, userRepositoryFile.findAll());
            }

        }

    }

    @Nested
    @DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
    @DisplayName("Method findById()")
    class MethodFindById {

        private List<UserEntity> users;

        @BeforeEach
        void init() {
            users = new ArrayList<>();
            users.add(new UserEntity(1, "Имя1", "Фамилия1", 10, false));
            users.add(new UserEntity(2, "Имя2", "Фамилия2", 20, false));
            users.add(new UserEntity(3, "Имя3", "Фамилия3", 30, true));
            users.add(new UserEntity(4, "Имя4", "Фамилия4", 40, true));
            users.add(new UserEntity(5, "Имя5", "Фамилия5", 50, true));

            doReturn(users).when(driverMock).readFromFile();
        }

        @Nested
        @DisplayName("should be successful")
        class ShouldBeSuccessful {

            @ParameterizedTest
            @ValueSource(ints = {1, 2, 3, 4, 5})
            void when_user_with_id_exists(int userId) {
                assertEquals(users.get(userId - 1), userRepositoryFile.findById(userId));
            }

        }

        @Nested
        @DisplayName("should throw UserException")
        class ShouldThrowUserException {

            @ParameterizedTest
            @ValueSource(ints = {-2, -1, 0, 6, 7})
            void when_user_with_id_does_not_exist(int userId) {
                UserException thrownException = assertThrows(
                        UserException.class,
                        () -> userRepositoryFile.findById(userId)
                );

                assertEquals(
                        String.format("Пользователь с ID = %1$d не найден!", userId),
                        thrownException.getMessage()
                );
            }

        }

    }

    @Nested
    @DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
    @DisplayName("Method create()")
    class MethodCreate {

        private List<UserEntity> users;

        @BeforeEach
        void init() {
            users = new ArrayList<>();
            users.add(new UserEntity(1, "Имя1", "Фамилия1", 10, false));
            users.add(new UserEntity(2, "Имя2", "Фамилия2", 20, false));
            users.add(new UserEntity(3, "Имя3", "Фамилия3", 30, true));
            users.add(new UserEntity(4, "Имя4", "Фамилия4", 40, true));
            users.add(new UserEntity(5, "Имя5", "Фамилия5", 50, true));

            doReturn(users).when(driverMock).readFromFile();
        }

        @Nested
        @DisplayName("should be successful")
        class ShouldBeSuccessful {

            @Test
            void when_user_with_id_exists() {
                UserEntity user = new UserEntity(6, "Имя6", "Фамилия6", 71, true);
                //добавить пользователя в файл (добавить его в список)

                assertEquals(users.get(userId - 1), userRepositoryFile.findById(userId));
            }

        }

        @Nested
        @DisplayName("should throw UserException")
        class ShouldThrowUserException {

            @ParameterizedTest
            @ValueSource(ints = {-2, -1, 0, 6, 7})
            void when_user_with_id_does_not_exist(int userId) {
                UserException thrownException = assertThrows(
                        UserException.class,
                        () -> userRepositoryFile.findById(userId)
                );

                assertEquals(
                        String.format("Пользователь с ID = %1$d не найден!", userId),
                        thrownException.getMessage()
                );
            }

        }

    }
}