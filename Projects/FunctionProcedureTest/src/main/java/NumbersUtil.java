package TestApp.stc2213.FunctionProcedureTest;

public interface NumbersUtil {
    boolean isPrime(int number);
    int gcd(int a, int b);
}
