package TestApp.stc2213.FunctionProcedureTest;

class App {
    public static void main(String[] args) {
        // Область видимости
        int a = 196;
        int b = 54;
        int c = 678;
        // Язык со статической типизацией (текст, число, символ и т.д.)
        int result = gcdFunc(a, b);
        System.out.println("Функция вернула " + result);
        gcdProcedure(c, a);
    }

    /**
     * Greatest common divisor - наибольший общий делитель (НОД)
     * @param a - делимое
     * @param b - частное
     * @return - НОД типа int
     */
    public static int gcdFunc(int a, int b) {
        // Область видимости (локальная)
        while (b != 0) {
            int tmp = a % b;
            a = b;
            b = tmp;
        }
        //кое-то действие
        return a;
    }

    public static void gcdProcedure(int a, int b) {
        // Область видимости
        while (b != 0) {
            int tmp = a % b;
            a = b;
            b = tmp;
        }
        //кое-то действие
        System.out.println("Процедура посчитала результат " + a);
    }
}