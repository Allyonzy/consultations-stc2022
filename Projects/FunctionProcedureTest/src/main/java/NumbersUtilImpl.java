package TestApp.stc2213.FunctionProcedureTest;

public class NumbersUtilImpl implements NumbersUtil {
    public boolean isPrime(int number) {
        if (number == 0 || number == 1) {
            throw new IncorrectNumberException();
        }

        if (number == 2 || number == 3) {
            return true;
        }

        for (int i = 2; i * i <= number; i++) {
            if (number % i == 0) {
                return false;
            }
        }

        return true;
    }

    /**
     * Greatest common divisor - наибольший общий делитель (НОД)
     * @param a - делимое
     * @param b - частное
     * @return - НОД типа int
     */
    public int gcd(int a, int b) {
        // Область видимости
        while (b != 0) {
            int tmp = a % b;
            a = b;
            b = tmp;
        }
        return a;
    }
}
