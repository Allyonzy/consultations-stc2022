package ru.innopolis.app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtil {
    private static Connection con;

    public DBUtil() throws SQLException {
        con = createConnection();
    }

    public static Connection createConnection() throws SQLException {
        return DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/accounts",
                "postgres",
                "qweasd10"
        );
    }

    public static Connection getCon() {
        return con;
    }

    public static void setCon(Connection con) {
        DBUtil.con = con;
    }
}
