package ru.innopolis.app;

import ru.innopolis.jpa.User;
import ru.innopolis.repository.UserRepository;
import ru.innopolis.repository.UserRepositoryJdbcApiImpl;

import java.sql.Connection;
import java.sql.SQLException;

public class Main {
    private static Connection con;

    public static void main(String[] args) throws SQLException {
        UserRepository userRepository = new UserRepositoryJdbcApiImpl(DBUtil.getCon());
        System.out.println(userRepository.findAll());

        User pavlik = new User("Павлик", "Сапковский", 27);

        userRepository.create(pavlik);

        System.exit(0);

    }
}
