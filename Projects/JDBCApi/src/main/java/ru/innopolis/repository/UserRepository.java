package ru.innopolis.repository;

import ru.innopolis.jpa.User;

import java.util.List;

public interface UserRepository {
    /**
     * Найти всех пользователей
     * @return
     */
    List<User> findAll();

    /**
     * Найти всех пользователей по идентификатору
     * @param id
     * @return
     */
    User findById(int id);

    /**
     * Создать нового пользователя
     * @param user
     */
    void create(User user);

    /**
     * Обновить параметры пользователя
     * @param user
     */
    void update(User user);

    /**
     * Удалить пользователя по идентификатору
     * @param user
     */
    void delete(User user);
}
