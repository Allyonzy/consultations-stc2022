package ru.innopolis.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Создан для преобразования объекта в строку БД и наоборот
 * Есть по умолчанию в org.springframework spring-jdbc
 * Но она нам тут не нужна
 * @param <T>
 */
@FunctionalInterface
public interface RowMapper <T> {
    T mapRow(ResultSet row) throws SQLException;
}
