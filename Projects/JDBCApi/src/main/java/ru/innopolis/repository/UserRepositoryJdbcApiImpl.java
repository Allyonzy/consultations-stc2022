package ru.innopolis.repository;

import ru.innopolis.jpa.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryJdbcApiImpl implements UserRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from account";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from account where id = ?";

    //language=SQL
    private static final String SQL_INSERT = "insert into account(first_name, last_name, age) values (?, ?, ?)";

    //language=SQL
    private static final String SQL_UPDATE = "update account set first_name = ?, last_name = ?, age = ? where id = ?";

    //language=SQL
    private static final String SQL_DELETE = "delete from account where id = ?";

    private final Connection connection;

    public UserRepositoryJdbcApiImpl(Connection connection) {
        this.connection = connection;
    }

    private final RowMapper<User> userRowMapper = row -> new User(
            row.getInt("id"),
            row.getString("first_name"),
            row.getString("last_name"),
            row.getInt("age")
    );

    @Override
    public List<User> findAll() {
        Statement statement = null;
        ResultSet rows = null;

        try {
            List<User> users = new ArrayList<>();
            statement = connection.createStatement();
            rows = statement.executeQuery(SQL_SELECT_ALL);

            while (rows.next()) {
                User user = userRowMapper.mapRow(rows);
                users.add(user);
            }

            return users;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            if(rows != null) {
                try {
                    rows.close();
                } catch (SQLException throwables) {
                    //проигнорируем
                }
            }

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException throwables) {
                    //проигнорируем
                }
            }
        }
    }

    @Override
    public User findById(int id) {
        return null;
    }

    @Override
    public void create(User user) {
        PreparedStatement statement = null;
        ResultSet generatedKeys = null;

        try {
            statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, user.getFirstName());
            statement.setString(2, user.getLastName());
            statement.setInt(3, user.getAge());

            int affectedRows = statement.executeUpdate(); //отрабатывает запрос update

            if(affectedRows != 1) {
                throw new SQLException("Невозможно добавить пользователя");
            }

            //сгенерировать ключи для запроса
            generatedKeys = statement.getGeneratedKeys();

            if(generatedKeys.next()) {
                user.setId(generatedKeys.getInt("id"));
            } else {
                throw new SQLException("Невозможно задать идентификатор пользователю");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            if(generatedKeys != null) {
                try {
                    generatedKeys.close();
                } catch (SQLException throwables) {
                    //проигнорируем
                }
            }

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException throwables) {
                    //проигнорируем
                }
            }
        }

    }

    @Override
    public void update(User user) {

    }

    @Override
    public void delete(User user) {

    }
}
