package ru.innopolis.spring.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "subscriber")
public class Subscriber {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "last_name")
    private String lastName;

    private String address;

    @Column(name = "telephone_number")
    private String telephoneNumber;

    @Column(name = "has_discount")
    private Boolean hasDiscount;

    @OneToMany(mappedBy = "subscriber")
    private List<Payment> payments;

    public Subscriber() {}

    public Subscriber(Integer id,
                      String firstName,
                      String lastName,
                      String address,
                      String telephoneNumber,
                      Boolean hasDiscount) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.telephoneNumber = telephoneNumber;
        this.hasDiscount = hasDiscount;
    }

    public void setMiddleName(String middle_name) {
        this.middleName = middle_name;
    }

    @Override
    public String toString() {
        return "Subscriber{" +
                "id=" + id +
                ", first_name='" + firstName + '\'' +
                ", middle_name='" + middleName + '\'' +
                ", last_name='" + lastName + '\'' +
                ", address='" + address + '\'' +
                ", telephone_number='" + telephoneNumber + '\'' +
                ", has_discount=" + hasDiscount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Subscriber)) return false;
        Subscriber that = (Subscriber) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }


}
