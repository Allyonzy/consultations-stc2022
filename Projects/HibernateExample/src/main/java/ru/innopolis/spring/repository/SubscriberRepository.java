package ru.innopolis.spring.repository;

import ru.innopolis.jpa.model.Subscriber;

public interface SubscriberRepository {
    void save(Subscriber subscriber);
}
