package ru.innopolis.spring.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.innopolis.jpa.repositories.paymentRepository.PaymentRepository;
import ru.innopolis.jpa.repositories.subscriberRepository.SubscriberRepository;
import ru.innopolis.spring.config.JpaConfig;

public class Applicetion {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(JpaConfig.class);
        SubscriberRepository subscriberRepository = context.getBean(SubscriberRepository.class);
        PaymentRepository paymentRepository = context.getBean(PaymentRepository.class);

        //создание объектов и сохранение
    }
}
