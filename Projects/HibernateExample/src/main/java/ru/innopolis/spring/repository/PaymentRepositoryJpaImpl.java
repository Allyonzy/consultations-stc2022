package ru.innopolis.spring.repository;

import org.springframework.stereotype.Repository;
import ru.innopolis.jpa.model.Payment;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class PaymentRepositoryJpaImpl implements PaymentRepository {

    //language=SQL
    private static final String SELECT_ALL_BY_SUBSCRIBER_ID = "select payment from Payment payment where payment.subscriber.id = ?1";

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void save(Payment payment) {
        entityManager.persist(payment);
    }

    @Override
    public List<Payment> findAllBySubscriberId(Integer subscriberId) {
        return entityManager.createQuery(SELECT_ALL_BY_SUBSCRIBER_ID, Payment.class)
                .setParameter(1, subscriberId)
                .getResultList();
    }
}
