package ru.innopolis.spring.model;

public enum Rate {
    BASIC,
    SILVER,
    GOLD
}

