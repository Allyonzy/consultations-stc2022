package ru.innopolis.spring.repository;

import org.springframework.stereotype.Repository;
import ru.innopolis.jpa.model.Subscriber;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Repository
public class SubscriberRepositoryJpaImpl implements SubscriberRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void save(Subscriber subscriber) {
        entityManager.persist(subscriber);
    }
}
