package ru.innopolis.jpa.app;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.innopolis.jpa.repositories.paymentRepository.PaymentRepository;
import ru.innopolis.jpa.repositories.paymentRepository.PaymentRepositoryJpaImpl;
import ru.innopolis.jpa.repositories.subscriberRepository.SubscriberRepository;
import ru.innopolis.jpa.repositories.subscriberRepository.SubscriberRepositoryJpaImpl;

import javax.persistence.EntityManager;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("HibernateExample\\src\\main\\resources\\hibernate\\hibernate.cfg.xml");

        SessionFactory sessionFactory = configuration.buildSessionFactory();
        EntityManager entityManager = sessionFactory.createEntityManager();

        SubscriberRepository subscriberRepository = new SubscriberRepositoryJpaImpl(entityManager);
        PaymentRepository paymentRepository = new PaymentRepositoryJpaImpl(entityManager);

        //создать и сохранить в репозиторий

    }
}
