package ru.innopolis.jpa.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Entity
public class Payment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private LocalDate datePayment;
    private Rate rate;
    private Integer payment;
    private LocalDate dateDisconnect;

    @ManyToOne
    @JoinTable(
            joinColumns = @JoinColumn(name="subscriber_id", referencedColumnName = "id")
    )
    private Subscriber subscriber;

    public Payment() {}

    public Payment(Integer id, Rate rate, Integer payment, Subscriber subscriber) {
        this.id = id;
        this.rate = rate;
        this.payment = payment;
        this.subscriber = subscriber;
    }

    public void setDatePayment(LocalDate datePayment) {
        this.datePayment = datePayment;
    }

    public void setDateDisconnect(LocalDate dateDisconnect) {
        this.dateDisconnect = dateDisconnect;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "id=" + id +
                ", datePayment=" + datePayment +
                ", rate=" + rate +
                ", payment=" + payment +
                ", dateDisconnect=" + dateDisconnect +
                ", subscriber=" + subscriber +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Payment)) return false;
        Payment payment1 = (Payment) o;
        return Objects.equals(id, payment1.id) && Objects.equals(datePayment, payment1.datePayment) && rate == payment1.rate && Objects.equals(payment, payment1.payment) && Objects.equals(dateDisconnect, payment1.dateDisconnect) && Objects.equals(subscriber, payment1.subscriber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, datePayment, rate, payment, dateDisconnect, subscriber);
    }


}
