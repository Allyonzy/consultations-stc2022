package ru.innopolis.jpa.repositories.paymentRepository;

import ru.innopolis.jpa.model.Payment;

import java.util.List;

public interface PaymentRepository {
    /**
     * Сохранение платежа
     * @param payment
     */
    void save(Payment payment);

    /**
     * Поиск по ЛС
     * @param subscriberId
     * @return
     */
    List<Payment> findAllBySubscriberId(Integer subscriberId);
}
