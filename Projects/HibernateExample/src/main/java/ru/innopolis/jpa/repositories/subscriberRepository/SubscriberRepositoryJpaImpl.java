package ru.innopolis.jpa.repositories.subscriberRepository;

import ru.innopolis.jpa.model.Subscriber;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class SubscriberRepositoryJpaImpl implements SubscriberRepository {

    private EntityManager entityManager;

    public SubscriberRepositoryJpaImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void save(Subscriber subscriber) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(subscriber);
        transaction.commit();
    }
}
