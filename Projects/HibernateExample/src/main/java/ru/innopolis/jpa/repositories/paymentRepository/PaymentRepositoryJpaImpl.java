package ru.innopolis.jpa.repositories.paymentRepository;

import ru.innopolis.jpa.model.Payment;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

public class PaymentRepositoryJpaImpl implements PaymentRepository {

    //language=SQL
    private static final String SELECT_ALL_BY_SUBSCRIBER_ID = "select payment from Payment payment where payment.subscriber.id = ?1";

    private EntityManager entityManager;

    public PaymentRepositoryJpaImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void save(Payment payment) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(payment);
        transaction.commit();
    }

    @Override
    public List<Payment> findAllBySubscriberId(Integer subscriberId) {
        return entityManager.createQuery(SELECT_ALL_BY_SUBSCRIBER_ID, Payment.class)
                .setParameter(1, subscriberId)
                .getResultList();
    }
}
