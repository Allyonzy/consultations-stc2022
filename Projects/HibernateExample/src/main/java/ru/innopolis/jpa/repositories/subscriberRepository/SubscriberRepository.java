package ru.innopolis.jpa.repositories.subscriberRepository;

import ru.innopolis.jpa.model.Subscriber;

public interface SubscriberRepository {
    void save(Subscriber subscriber);
}
