import model.*;
import repository.*;

import java.util.*;
import java.util.stream.Collectors;

public class MainDemo {

    public static void main(String[] args) {
        // dump файлы (загрузим информацию из txt)
        // сделать через форматы
        ReaderRepository readerRepository = new ReaderRepositoryImpl("stream_practice/src/data/input_reader.txt");
        BookRepositoryImpl bookRepository = new BookRepositoryImpl("stream_practice/src/data/input_book.txt");
        AuthorRepository authorRepository = new AuthorRepositoryImpl("stream_practice/src/data/input_author.txt");

        List<Reader> readers = readerRepository.findAll(); //readerRepository возвращает new ArrayList<>();
        List<Book> books = bookRepository.findAll();
        List<Author> authors = authorRepository.findAll();

        Library library = new Library(books, readers);

        // сортировка по году издания
        List<Book> sortedBooks = library
                .getBooks()
                .stream()
                .sorted(Comparator.comparing((Book book) -> book.getIssueYear()))
                .collect(Collectors.toList());

        List<String> filteredBooks = library
                .getBooks()
                .stream()
                .filter(book -> book.getIssueYear() == 2021)
                .map((Book book) -> book.getName())
                .collect(Collectors.toList());



        System.exit(0);

    }
}
