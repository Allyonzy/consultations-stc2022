package repository;

import model.Author;

import java.util.List;

public interface AuthorRepository {
    List<Author> findAll();
    void save(Author book);
    List<Author> findByInterest(String interest); //найти по жанру написания
    List<Author> findBySurname(String surname); //найти по фамилии
}
