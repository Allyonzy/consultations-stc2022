package repository;

import utils.FileController;
import model.EmailAddress;
import model.Reader;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ReaderRepositoryImpl implements ReaderRepository, FileController {

//    public static final String FILE_NAME = "stream_practice/src/data/input_reader.txt";

    public String fileName;

    public ReaderRepositoryImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<Reader> findAll() {
        List<String> data = readFile(this.fileName);
        List<Reader> readers = new ArrayList<>();

        data.forEach(line -> {
            String[] parts = line.split("\\|");
            readers.add(new Reader(parts[0], parts[1], Boolean.parseBoolean(parts[2])));
        });

        return readers;
    }

    @Override
    public List<EmailAddress> getEmailsList() {
        List<Reader> readers = findAll();

        if(readers.isEmpty()) {
            return null;
        }

        return readers
                .stream()
                .filter(reader -> reader.isSubscriber())
                .map(Reader::getEmail)
                .map(EmailAddress::new)
                .collect(Collectors.toList());
    }

    @Override
    public void save(Reader reader) {

    }

}
