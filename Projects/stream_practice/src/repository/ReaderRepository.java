package repository;

import model.EmailAddress;
import model.Reader;

import java.util.List;

public interface ReaderRepository {
    List<Reader> findAll();
    void save(Reader reader);
    List<EmailAddress> getEmailsList();
}
