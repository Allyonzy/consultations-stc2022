package repository;

import utils.FileController;
import model.Author;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class AuthorRepositoryImpl implements AuthorRepository, FileController {

//    public static final String fileName = "stream_practice/src/data/input_author.txt";

    public String fileName;

    public AuthorRepositoryImpl(String fileName) {
        this.fileName = fileName;
    }

    public List<Author> findAll() {
        List<String> data = readFile(this.fileName);

        List<Author> authors = new ArrayList<>();
        data.forEach(line -> {
            String[] parts = line.split("\\|");
            List<String> genresList = Arrays.asList(parts[2].split(" ,"));
            authors.add(new Author(parts[0], parts[1], genresList));
        });

        return authors;
    }

    @Override
    public void save(Author book) {

    }

    @Override
    public List<Author> findByInterest(String interest) {
        return null;
    }

    @Override
    public List<Author> findBySurname(String surname) {
        List<Author> authors = findAll();

        if (authors.isEmpty()) {
            return null;
        }

        return authors
                .stream()
                .filter(author -> author.getSurname().equals(surname))
                .collect(Collectors.toList());
    }
}
