package model;

import java.util.Objects;
import java.util.UUID;

public class Book {
    private UUID bookId; //Идентификатор
    private Author author; //Автор
    private String name;	//Название
    private Integer issueYear; //Год издания

    public Book() {
        this.bookId = UUID.randomUUID();
    }

    public Book(Author author, String name, Integer issueYear)   {
        this.bookId = UUID.randomUUID();
        this.author = author;
        this.name = name;
        this.issueYear = issueYear;
    }

    public UUID getBookId() {
        return bookId;
    }

    public Author getAuthor() {
        return author;
    }

    public String getName() {
        return name;
    }

    public Integer getIssueYear() {
        return issueYear;
    }

    @Override
    public String toString() {
        return "model.Book{" +
                "author='" + author + '\'' +
                ", name='" + name + '\'' +
                ", issueYear=" + issueYear +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Book book = (Book) o;
        return author.equals(book.author) &&
                name.equals(book.name) &&
                issueYear.equals(book.issueYear);
    }

    @Override
    public int hashCode() {
        return Objects.hash(author, name, issueYear);
    }
}